local sensorInfo = {
	name = "Wind",
	desc = "Return data of actual wind.",
	author = "PepeAmpere",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return position where the agent should move
return function()
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
    agentMoveVec = Vec3(normDirX, normDirY, normDirZ)
    agentMoveVec = agentMoveVec * 100
    return {
        agentMoveVec = agentMoveVec
	}
end